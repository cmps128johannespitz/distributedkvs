jpitz: wrote the 'view code' to group the nodes into partitions, adjsuted view_update to work with the new view, rewrote big parts of the 'asg2 code' to allow checking for newer values in partition and so on, wrote gossip function.

mdeyell: worked on implementation of vector clocks and passing causal history around, debugging, and identity api calls 

rymjacob: worked on implementation of vector clocks and passing causal history around, helped with re-write of re_hash and did a lot of debugging