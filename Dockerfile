FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev
RUN pip install Flask
RUN pip install requests
COPY ./webapp /app
WORKDIR /app
EXPOSE 8080
CMD ["python", "/app/app.py"]