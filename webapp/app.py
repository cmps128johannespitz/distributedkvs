import os, json, re, sys, threading, time, requests as req
from flask import Flask, request, jsonify, make_response
import hashlib

app = Flask(__name__)


# This is the Key-Value-Storage
kvs = dict()

#this is the vector clock, it stores data in the form of ip: clockvalue
vc = dict()

# 2^20
NUMBER = 4294967296

def gossip():
    while True:
        time.sleep(2)
        do_gossip()

thread = threading.Thread(name='gossip_thread', target=gossip)
thread.setDaemon(True)
thread.start()

def do_gossip():
    view = get_view_info()
    ip_port = os.environ['IPPORT']
    partition_id = get_partition_id()

    partition_members = view[partition_id]

    #  For every key in local kvs
    for key in kvs:
        # send local key to every other partion member
        for host in partition_members:
            if not (ip_port == host):
                url_str = 'http://' + host + '/kvs_update/' + key
                try:
                    req.put(url_str, data={'value': str(kvs[key]), 'ip_port': ip_port})
                except Exception as e:
                    continue

    return True

@app.route('/kvs_update/<the_key>', methods=['PUT'])
def update_value(the_key):
    me = os.environ['IPPORT']
    other = request.form['ip_port']
    value = eval(request.form['value'])
    print(value)
    sys.stdout.flush()

    # if they key isn't in the kvs already, put it there
    # this is only ok because we don't have to deal with deleting keys
    if not (the_key in kvs):
        kvs[the_key] = value
        return

    # if key is in kvs, and the recieved value is different from the local value
    # compare the VCs
    # if the received VC is small, do nothing
    # if the received VC is large, replace the key
    # if the received VC is equal to local VC, decide based on unique node IDs (ip_port)
    if not (value[0]==kvs[the_key][0]):
        if compareVC(value[1], kvs[the_key][1]) == 'greater':
            kvs[the_key] = value
            return
        if compareVC(value[1], kvs[the_key][1]) == 'lesser':
            return
        if value[2] > kvs[the_key][2]:
            kvs[the_key] = value
            return
        if value[2] < kvs[the_key][2]:
            return
        # here we assume unique ip_ports
        # not other > me ==> other < me
        if other > me:
            kvs[the_key] = value
    return


def vc_inc():
    ip_port = os.environ['IPPORT']
    if ip_port in vc:
        vc[ip_port] += 1
    else:
        vc[ip_port] = 1


# takes dict to compare to 
# if concurrent is returned, sync to whichever pairwise is greater
# if greater is returned, if vc_one > vc_two
# if lesser is returned, we should take the other nodes vc
def compareVC(vc_one, vc_two):
    view = get_view_info()
    partition = view[get_partition_id()]

    #1 variable not enough since 3 outcomes and 1 bit can only represent 2
    isGreater=True
    isLesser=True
    for host in partition:
        if (host in vc_one) and (host in vc_two): 
            if vc_one[host] > vc_two[host]:
                isLesser=False
            elif vc_one[host] < vc_two[host]:
                isGreater=False
        else:
            return "concurrent"

    if (isGreater == isLesser):
        return "concurrent"
    elif (isGreater):
        return "greater"
    else:
        return "lesser"



def vc_merge(invc):
    global vc
    view = get_view_info()
    partition = view[get_partition_id()]
    
    for host in partition:
        if (host in invc) and (host in vc):
            if vc[host] < invc[host]:
                vc[host] = invc[host]
        elif (host in invc):
            vc[host] = invc[host]


# add virtual nodes to the circle to improve hashing uniformity
def get_view():
    __view = os.environ['VIEW'].split(",")
    __view.sort()

    __k = int(os.environ['K'])

    # http://stackoverflow.com/questions/4998427/how-to-group-elements-in-python-by-n-elements
    # Credit to Mike DeSimone
    partitions = [__view[n:n+__k] for n in range(0, len(__view), __k)]

    view = []
    for partition in partitions:
        view.append( (int(hashlib.md5(partition[0]).hexdigest(),16) % NUMBER, partition) )

        view.append( (int(hashlib.md5('1234'+partition[0]).hexdigest(),16) % NUMBER, partition) )
        view.append( (int(hashlib.md5('abcd'+partition[0]).hexdigest(),16) % NUMBER, partition) )
        view.append( (int(hashlib.md5(partition[0]+'1234').hexdigest(),16) % NUMBER, partition) )
        view.append( (int(hashlib.md5(partition[0]+'abcd').hexdigest(),16) % NUMBER, partition) )

        view.append( (int(hashlib.md5('5678'+partition[0]).hexdigest(),16) % NUMBER, partition) )
        view.append( (int(hashlib.md5('xyz'+partition[0]).hexdigest(),16) % NUMBER, partition) )
        view.append( (int(hashlib.md5(partition[0]+'5678').hexdigest(),16) % NUMBER, partition) )
        view.append( (int(hashlib.md5(partition[0]+'xyz').hexdigest(),16) % NUMBER, partition) )

    view.sort()

    return view

def get_view_info():
    __view = os.environ['VIEW'].split(",")
    __view.sort()
    __k = int(os.environ['K'])

    # http://stackoverflow.com/questions/4998427/how-to-group-elements-in-python-by-n-elements
    # Credit to Mike DeSimone
    partitions = [__view[n:n+__k] for n in range(0, len(__view), __k)] 
    return partitions  


@app.route('/')
def hello():
    return os.environ.get('IPPORT', "no IPPORT found")

@app.route('/kvs/view', methods=['PUT'])
def update_nodes_view():
    new_view = request.form['view']
    os.environ['VIEW'] = new_view
    successfulView = {"msg": "view update successful", "view": new_view}
    return make_response(jsonify(successfulView),200)


@app.route('/view', methods=['GET'])
def show_view():
    msg = {"msg": "successful", "view": get_view()}
    return make_response(jsonify(msg),200)

@app.route('/test')
def test():
    msg = {"msg": "successful", "view": get_view_info()}
    return make_response(jsonify(msg),200)


@app.route('/kvs/get_partition_id', methods=['GET'])
def ret_partition_id():
    msg = {"msg": "success", "partition_id": get_partition_id()}
    return make_response(jsonify(msg),200)

def get_partition_id():
    view = get_view_info()
    ip_port = os.environ['IPPORT']
    partition_id = -1
    for partition in view:
        partition_id += 1
        if ip_port in partition:
            break
    return partition_id


@app.route('/kvs/get_all_partition_ids', methods=['GET'])
def get_all_partition_ids():
    view = get_view_info()
    partition_id = -1
    partition_id_list = []
    for partition in view:
        partition_id += 1
        partition_id_list.append(partition_id)
    msg = {"msg": "success", "partition_id_list": partition_id_list}
    return make_response(jsonify(msg),200)


@app.route('/kvs/get_partition_members', methods=['GET'])
def get_partition_members():
    view = get_view_info()
    partition_id = int(request.form['partition_id'])
    partition_members = view[partition_id]
    msg = {"msg": "success", "partition_members": partition_members}
    return make_response(jsonify(msg),200)


# Deal with putting a new node into the system
@app.route('/kvs/update_view', methods=['PUT'])
def update_view():
    instruction = request.args.get('type')
    ip_port = request.form['ip_port']
    old_view = os.environ.get('VIEW', '').split(",")

    if instruction == "add":
        # append new ip_port to VIEW environ
        os.environ['VIEW'] = os.environ['VIEW'] + "," + ip_port
        url_str = 'http://' + ip_port + '/kvs/view'
        try:
            resp = req.put(url_str, data={'view': os.environ['VIEW']}) 
        except Exception as e:
            dataError = {"msg": 'error', 'error': ip_port + ' did not update VIEW environment'}
            return make_response(jsonify(dataError),404)
    else:
        # delete ip_port from VIEW, have to deal with separating commas
        view = os.environ.get('VIEW', '').split(",")
        view_index = view.index(ip_port)
        if view_index == 0:
            removed_ipport = ip_port + ","
        else:
            removed_ipport = "," + ip_port
        
        # change the VIEW environ
        os.environ['VIEW'] = os.environ['VIEW'].replace(removed_ipport, '')
        # old_view = os.environ['VIEW'].split(",")

    # communicate the change in VIEW to the rest of the nodes
    current_host = os.environ.get('IPPORT') 
    for host in old_view:
        if host != current_host:
            url_str = 'http://' + host + '/kvs/view'
            try:
                resp = req.put(url_str, data={'view': os.environ.get('VIEW')}, timeout=0.5)
            except Exception as e:
                if (host == ip_port):
                    continue
                else:
                    dataError = {"msg": 'error', 'error': host + ' did not update VIEW environment'}
                    return make_response(jsonify(dataError),404)

    # once VIEW change has spread to all nodes, re-hash all keys and move them around
    for host in old_view:
        url_str = 'http://' + host + '/kvs/re_hash'
        try:
            resp = req.get(url_str, timeout=(0.5,30))
        except Exception as e:
                if (host == ip_port):
                    continue
                else:
                    dataError = {"msg": 'error', 'error': host + ' did not update keys'}
                    return make_response(jsonify(dataError),404)


    view = get_view_info()
    partition_count = 0        
    for partition in view:
        partition_count += 1

    if instruction == "add":
        # ip_port = request.form['ip_port']
        partition_id = get_partition_id()
        resp = {"msg": "success", "partition_id": partition_id, "number_of_partitions": partition_count}
    else:
        resp = {"msg": "success", "number_of_partitions": partition_count}
    return make_response(jsonify(resp),200)


# move nodes around after adding or deleting a node
@app.route('/kvs/re_hash', methods=['GET'])
def re_hash():
    current_host = os.environ.get('IPPORT')  
    view = get_view() 
    toDel = [] # list of keys (per node) that must move (and be deleted) from current node

    # use the same hashing function as in action() to re-hash the key
    # do not take any action if the key's owner (the host) remains the same
    for key in kvs:
        goes_in_first_partition = 1
        for partition in view:
            if int(hashlib.md5(key).hexdigest(),16) % NUMBER < partition[0]:
                goes_in_first_partition = 0
                if not (current_host in partition[1]):   # if current host is not in partition, then delete
                    toDel.append(key)
                for host in partition[1]:
                    if not (current_host == host):
                        url_str = 'http://' + host + '/kvs_update/' + key
                        try:
                            req.put(url_str, data={'value': str(kvs[key]), 'ip_port': current_host})
                        # url_str = 'http://' + host + '/kvs/' + key
                        # try:
                        #     resp = req.put(url_str, data={'causal_payload': json.dumps(vc), 'val': kvs[key][0]})
                        except Exception as e:
                            # make the error message if the receiver node is down
                            dataError = {"msg": 'error', 'error': 'service is not available1'}
                            return make_response(jsonify(dataError), 404)
                break
        # hash is greater than any of them, use the first in the ring
        if (goes_in_first_partition == 1):
            if not (current_host in view[0][1]):   # if current host is not in partition, then delete
                toDel.append(key)
            for host in view[0][1]:
                if not (current_host == host):
                    url_str = 'http://' + host + '/kvs_update/' + key
                    try:
                        req.put(url_str, data={'value': str(kvs[key]), 'ip_port': current_host})
                    # url_str = 'http://' + host + '/kvs/' + key
                    # try:
                    #     resp = req.put(url_str, data={'causal_payload': json.dumps(vc), 'val': kvs[key]})
                    except Exception as e:
                        # make the error message if the receiver node is down
                        dataError = {"msg": 'error', 'error': 'service is not available2'}
                        return make_response(jsonify(dataError), 404)
            

    # delete all the local keys that have been moved to a different node
    for item in toDel:
        del kvs[item]

    resp = {"msg": 'success', "ip : port": current_host, "view": view}
    return make_response(jsonify(resp),200)

# Deal with requests to get/put a key 
# Delete functions are not reachable any more
@app.route('/kvs/<the_key>', methods=['GET', 'PUT'])
def main(the_key): 

    #key error checking
    if not(re.match('^[a-zA-Z0-9_]+$',the_key)) or len(the_key) > 250 or len(the_key) < 1:
        dataError = {"msg": 'error', 'error': 'key not valid'}
        return make_response(jsonify(dataError), 400)
    #data error checking
    if request.method == 'PUT':
        try: 
            if  sys.getsizeof(request.form['val']) > 1.5e+6: 
                dataError = {"msg": 'error', 'error': 'data not valid'}
                return make_response(jsonify(dataError), 400)
        except Exception as e:
            dataError = {"msg": 'error', 'error': 'no value provided'}
            return make_response(jsonify(dataError), 400)

    view = get_view()
    for partition in view:
        if int(hashlib.md5(the_key).hexdigest(),16) % NUMBER < partition[0]:
            return action(partition[1], the_key)    
    # hash is greater than any of them, use the first in the ring
    return action(view[0][1], the_key)


# first test if the host we're given is the one we want
# if not, send the request onward, and then wait for a response to return
def action(partition, the_key):
    ip_port = os.environ.get('IPPORT', '')
    if ip_port in partition:
        if request.method == 'GET':
            return  get_value(the_key)
        if request.method == 'PUT':
            return  put_value(the_key)
        if request.method == 'DELETE':
            return  del_value(the_key)
    else:
        resp = None
        for host in partition:
            url_str = 'http://' + host + '/kvs/' + the_key
            try:
                if request.method == 'GET':
                    resp = req.get(url_str, data={'causal_payload': request.form['causal_payload']}, timeout=(0.15, 1))
                if request.method == 'PUT':
                    resp = req.put(url_str, data={'causal_payload': request.form['causal_payload'], 'val': request.form['val']}, timeout=(0.15, 1))
                if request.method == 'DELETE':
                    resp =  req.delete(url_str)
                break
            except Exception as e:
                continue

        if resp == None:
            dataError = {"msg": 'error', 'error': 'service not available'}
            return make_response(jsonify(dataError), 404)
        else:
            return make_response(resp.content, resp.status_code)


@app.route('/kvs_force/<the_key>', methods=['GET'])
def force_get_value(the_key):
    print('hit')
    if the_key in kvs:
        return str(kvs[the_key])
    else:
        return '{}'

def get_value(the_key):
    ip_port = os.environ.get('IPPORT', '')
    view = get_view_info()
    partition_id = get_partition_id()
    # vc_received = eval(request.form['causal_payload'])

    values = dict()
    if the_key in kvs: 
        values[ip_port] = kvs[the_key]
    for host in view[partition_id]:
        print(host)
        if not host == ip_port:
            url_str = 'http://' + host + '/kvs_force/' + the_key
            try:
                resp = eval(req.get(url_str, timeout=0.15).content)   
                if resp:
                    values[host] = resp                 
            except Exception as e:
                print(e)
                continue

    if not values:
        dataError = {"msg": 'error', 'error': 'key cannot be found'}
        return make_response(jsonify(dataError), 404)

    index, value = values.popitem()
    for host in values:
        if compareVC(values[host][1], value[1]) == 'greater':
            value = values[host]
            continue
        if compareVC(values[host][1], value[1]) == 'concurrent' and values[host][2] > value[2]:
            value = values[host]
            continue
        # here we assume unique ip_ports
        # not other > me ==> other < me
        if host > ip_port:
            value = values[host]

    successfulLookUp = {
        "msg": 'success', 
        'value': str(value[0]), 
        'partition_id': partition_id, 
        'causal_payload': str(value[1]),
        'timestamp': str(value[2])
    }  
    return make_response(jsonify(successfulLookUp), 200) 


@app.route('/kvs_force/<the_key>', methods=['PUT'])
def force_put_value(the_key):
    if request.form['causal_payload'] == '':
        vc_received = {}
    else:
        vc_received = eval(request.form['causal_payload'])
    vc_merge(vc_received)

    value = dict()
    value[0] = request.form['val']
    value[1] = vc
    value[2] = time.time()

    kvs[the_key] = value

    return str(value)



def put_value(the_key):
    ip_port = os.environ.get('IPPORT', '')
    view = get_view_info()
    partition_id = get_partition_id()
    if request.form['causal_payload'] == '':
        vc_received = {}
    else: 
        vc_received = eval(request.form['causal_payload'])

    print(vc_received)


    if (the_key in kvs): 
        status_code = 200
    else:
        status_code = 201

    vc_merge(vc_received)
    vc_inc()

    for host in view[partition_id]:
        if not host == ip_port:
            url_str = 'http://' + host + '/kvs_force/' + the_key
            try:
                req.put(url_str, data={'causal_payload': str(vc), 'val': request.form['val']}, timeout=0.1)                   
            except Exception as e:
                continue

    value = dict()
    value[0] = request.form['val']
    value[1] = vc
    value[2] = time.time()

    kvs[the_key] = value
    successfulLookUp = {
        "msg": 'success', 
        'partition_id': partition_id, 
        'causal_payload': str(value[1]),
        'timestamp': str(value[2])
    }
   
    return make_response(jsonify(successfulLookUp), status_code)


def del_value(the_key):
    ip_port = os.environ.get('IPPORT', '')
    if (the_key in kvs) == True:
        del kvs[the_key]
        successfulDelete = {"msg": 'success', "owner": ip_port}
        return make_response(jsonify(successfulDelete),200)
    else:
        dataError = {"msg": 'error', 'error': 'key does not exist', "owner": ip_port}
        return make_response(jsonify(dataError), 404)

 
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 8080))
    ip_port = os.environ['IPPORT']
    vc[ip_port] = 0
    app.run(host='0.0.0.0', port=port, threaded=True) #threaded=True necessary to deal with multiple requests at same time
